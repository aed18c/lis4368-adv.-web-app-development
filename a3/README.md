> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Allyson Davis

### Assignment #3 Requirements:

*Deliverables:*

1. Entity Relationship Diagram (ERD)
2. Include data (at least 10 records in each table)
3. Read only access to Bitbucket Repo
4. Links to a3.mwb and a3.sql

#### README.md file should include the following items:

* Links to a3.mwb and a3.sql
* Screenshot of a3 ERD based upon requirements

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshot and Links:
*Screenshot A3 ERD*:
![A3 ERD](img/a3.png "ERD based upon A3 Requirements")

*A3 docs: a3.mwb and a3.sql*:

[A3 MWB File](docs/a3.mwb "A3 ERD in .mwb format")

[A3 SQL File](docs/a3.sql "A3 SQL Script")
