> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Allyson Davis	

### Assignment #5 Requirements:

*Deliverables:*

1. Create new data package in crud subdirectory
2. Create and compile ConnectionPool.java
3. Create and compile CustomerDB.java
4. Create and compile DBUtil.java
5. Modify and compile CustomerServlet.java
6. Provide Screenshots of Passed Validation 
7. Sreenshot of updated database


#### README.md file should include the following items:

* Screenshot of Populated Form
* Screenshot of Passed Server-side validation
* Screenshot of Updated Database

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshot of Populated Form*:          |  *Screenshot of Passed Server-side validation*:
:-------------------------:|:-------------------------:
![Screenshot of Populated Form](img/form.PNG) | ![Screenshot of Passed Server-side Validation](img/passed.PNG)


*Screenshot of Updated Database*:

![Screenshot of Updated Database](img/database.PNG)



#### Links:

*Bitbucket Repo:*
[lis4368 Bitbucket Repo](https://bitbucket.org/aed18c/lis4368/ "lis4368 Bitbucket Repo")

*LIS 4368 Local Link:*
[localhost:9999/lis4368](http://localhost:9999/lis4368/ "localhost:9999/lis4368")
