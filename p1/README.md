> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Allyson Davis

### Project 1 Requirements:

*Deliverables:*

1. Modify index.jsp
2. Add jQuery validation and regular expressions
3. Use regexp to only allow appropriate characters for each control
4. Research validation codes
5. Screenshots of Failed and Passed Validations

#### README.md file should include the following items:

* Failed Validation Screenshot
* Passed Validation Screenshot

> 
> 



#### Project 1 Screenshot and Links:
*Screenshot of Failed Validation*:
![Failed Validation](img/fail.PNG "Failed Form Validation")

*Screenshot of Passed Validation*:
![Passed Validation](img/valid.PNG "Passed Form Validation")


*Project 1 Link*:
[P1 Local lis4368 WebApp](http://localhost:9999/lis4368/p1/index.jsp "P1 local lis4368 web app")
