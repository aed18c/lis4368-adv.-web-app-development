> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Allyson Davis	

### Assignment #4 Requirements:

*Deliverables:*

1. Modify customerform.jsp and thanks.jsp
2. Add jQuery validation and regular expressions
3. Use regexp to only allow appropriate characters for controls
4. Modify and compile CustomerServlet.java
5. Modify and compile Customer.java
6. Provide screenshots of Failed and Passed Validations


#### README.md file should include the following items:

* Screenshot of Failed Server-side Validation
* Screenshot of Passed Server-side validation

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshot of Failed Validation*:            |  *Screenshot of Passed Validation*:
:-------------------------:|:-------------------------:
![Screenshot of Failed Server-side Validation](img/failed.PNG) | ![Screenshot of Passed Server-side Validation](img/passed.PNG)


#### Links:

*Bitbucket Repo:*
[lis4368 Bitbucket Repo](https://bitbucket.org/aed18c/lis4368/ "lis4368 Bitbucket Repo")

*LIS 4368 Local Link:*
[localhost:9999/lis4368](http://localhost:9999/lis4368/ "localhost:9999/lis4368")
