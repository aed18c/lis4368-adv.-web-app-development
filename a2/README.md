> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Allyson Davis

### Assignment 2 Requirements:

*Includes:*

1. MySQL Login
2. Install MySQL JDBC Driver
3. Create ebookshop database
4. Create and Compile HelloServlet and QuerybookServlet
5. Connect Database Using Servlets

#### README.md file should include the following items:

* localhost:9999/hello Screenshot and link
* localhost:9999/hello/sayhello Screenshot and link
* localhost:9999/hello/querybook.html Screenshot and link
* One Screenshot of the query results

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### localhost:9999 links:

* (http://localhost:9999/hello)
* (http://localhost:9999/hello/HelloHome.html)
* (http://localhost:9999/hello/index.html)
* (http://localhost:9999/hello/sayhello)
* (http://localhost:9999/hello/querybook.html)

#### Assignment Screenshots:

*Screenshot of http://localhost:9999/hello*:
*Directory listed because of HelloHome.html*

![/hello screenshot](img/hello.png)

*Screenshot of http://localhost:9999/hello/index.html*:

![/hello/index.html screenshot](img/indexhtml.png)

*Screenshot of http://localhost:9999/hello/sayhello*:

![/hello/sayhello screenshot](img/sayhello.png)

*Screenshot of http://localhost:9999/hello/querybook.html*:

![/hello/querybook.html screenshot](img/querybook.png)

*Screenshot of Querybook Result*:

![querybook result Screenshot](img/queryresult.png)




#### Repo Links:

*Link to lis4368 BitBucket Repo*
[lis4368 BitBucket Repo](https://bitbucket.org/aed18c/lis4368/src/master/ "lis4368 Bitbucket repo")

*Link to Local lis4368 Web App:*
[A2 Local lis4368 web app](http://localhost:9999/lis4368/a2/index.jsp "A2 local lis4368 web app")
