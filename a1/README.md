> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Allyson Davis
### Assignment 1 Requirements:

*Includes:*

1. Git and BitBucket Tutorials
2. Development Installations
3. BitBucket Repo links

#### README.md file should include the following items:

* Bullet-list items
* Screenshot of AMPPS Install
* Screenshot of Java Hello
* Screenshot of running localhost:9999
* git commands with short descriptions

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init: creates an empty git repository or reinitializes an existing one
2. git status: displays the state of the working directory and lets you see which changes have been staged, which haven't, and which files aren't being tracked
3. git add: adds a change in the working directory to the staging area
4. git commit: used to save changes to the local Git repository
5. git push: used to upload local repository content to a remote repository
6. git pull: used to fetch and download content from a remote repository and update the local repository to match that content
7. One additional command - git clone: used to copy a git repository from a remote source and sets the remote to original source so you can pull again

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/ampps.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdkinstall.PNG)

*Screenshot of running https://localhost:9999*:

![Tomcat Installation Screenshot](img/tomcat.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/aed18c/bitbucketstationlocations/ "Bitbucket Station Locations")

*Link to Local lis4368 web app*
[A1 Local lis4368 web app](http://localhost:9999/lis4368/# "Local lis4368 web app")