> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS3468 - Advanced Web Applications

## Allyson Davis

### LIS4368 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install AMMPS
    - Install JDK
    - Install Tomcat
    - Provide screenshots of installations
    - Creat Bitbucket repo
    - Complete Bitbucket tutorials
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Log in to MySQL
    - Install MySQL JDBC Driver
    - Create ebookshop Database
    - Create and Compile HelloServlet and QuerybookServlet
    - Connect Database Using Servlets

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create Entity Relationship Diagram (ERD)
    - Include data (at least 10 records in each table)
    - Provide Read only access to Bitbucket Repo
    - Forward Engineer .mwb file to .sql file

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Modify customerform.jsp and thanks.jsp
    - Add jQuery validation and regular expressions
    - Use regexp to only allow appropriate characters for controls
    - Modify and compile CustomerServlet.java
    - Modify and compile Customer.java
    - Provide screenshots of Failed and Passed Validations

5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Create new data package in crud subdirectory
    - Create and compile ConnectionPool.java
    - Create and compile CustomerDB.java
    - Create and compile DBUtil.java
    - Modify and compile CustomerServlet.java
    - Provide Screenshots of Passed Validation 
    - Sreenshot of updated database

6. [P1 README.md](p1/README.md "My P1 README.md file")
    - Modify index.jsp
    - Add jQuery validation and regular expressions
    - Use regexp to only allow appropriate characters for each control
    - Research validation codes
    - Provide screenshots of Failed and Passed Validations

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Modify and Compile CustomerServlet.java
    - Modify and Compile CustomerDB.java
    - Use regexp to only allow appropriate characters for each control
    - Link database
    - Provide Screenshots demonstrating inserting, updating, and deleting abilities
